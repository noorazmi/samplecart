package com.sample.cart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.cart.R;
import com.sample.cart.customviews.NumberPicker;
import com.sample.cart.fragments.ProductListFragment;
import com.sample.cart.models.Cart;
import com.sample.cart.models.Product;
import com.sample.cart.utils.Constants;

import java.util.List;

/**
 * <p/>
 * Created by: Noor  Alam on 06/06/16.<br/>
 * Email id: noor.alam@tothenew.com<br/>
 * Skype id: mfsi_noora
 * <p/>
 */
public class CartProductListAdapter extends BaseAdapter{

    private final List<Product> mProductList;
    private final LayoutInflater mLayoutInflater;
    private final ProductListFragment.OnCartItemChangeListener mOnCartItemChangeListener;

    public CartProductListAdapter(Context context, List<Product> productList, ProductListFragment.OnCartItemChangeListener mOnCartItemChangeListener) {
        this.mProductList = productList;
        this.mOnCartItemChangeListener = mOnCartItemChangeListener;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return mProductList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.list_item_product, null);
            viewHolder = new ViewHolder(convertView);
            // store the holder with the view.
            convertView.setTag(viewHolder);
        }else {
            // we've just avoided calling findViewById() on resource every time by just using the viewHolder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Product product = (Product) getItem(position);
        viewHolder.mProduct = product;
        viewHolder.mImageViewIcon.setImageResource(product.getImageId());
        viewHolder.mTextViewProductName.setText(product.getName());
        viewHolder.mTextViewUnit.setText(product.getUnit());
        viewHolder.mTextViewPrice.setText(Constants.GBP_SYMBOL +String.valueOf(product.getPrice()));
        viewHolder.mNumberPickerProductCounter.setValue(Cart.getCart().getProductsQuantity(product));
        viewHolder.mNumberPickerProductCounter.setMinimumValue(1);

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Notify the fragment callbacks interface that an item has been clicked
            }
        });

        viewHolder.mNumberPickerProductCounter.setOnCounterValueChangeListener(new NumberPicker.OnCounterValueChangeListener() {
            @Override
            public void onCounterValueChange(int newCounterValue) {
                mOnCartItemChangeListener.onCartItemChange(viewHolder.mProduct, newCounterValue);
            }
        });

        viewHolder.mImageViewRemove.setVisibility(View.VISIBLE);
        viewHolder.mImageViewRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnCartItemChangeListener.onCartItemChange(viewHolder.mProduct, 0);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        public final View mView;
        public final ImageView mImageViewIcon;
        public final TextView mTextViewProductName;
        public final TextView mTextViewPrice;
        public final TextView mTextViewUnit;
        public final NumberPicker mNumberPickerProductCounter;
        public final ImageView mImageViewRemove;
        public Product mProduct;

        public ViewHolder(View view) {
            mView = view;
            mImageViewIcon = (ImageView) view.findViewById(R.id.imageview_icon);
            mTextViewProductName = (TextView) view.findViewById(R.id.textview_product_name);
            mTextViewPrice = (TextView) view.findViewById(R.id.textview_total_amount);
            mTextViewUnit = (TextView) view.findViewById(R.id.textview_unit);
            mImageViewRemove = (ImageView) view.findViewById(R.id.imageview_remove);
            mNumberPickerProductCounter = (NumberPicker) view.findViewById(R.id.number_picker_item_counter);
        }
    }
}
