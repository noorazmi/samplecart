package com.sample.cart.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.cart.R;
import com.sample.cart.customviews.NumberPicker;
import com.sample.cart.fragments.ProductListFragment;
import com.sample.cart.models.Cart;
import com.sample.cart.models.Product;
import com.sample.cart.utils.Constants;

import java.util.List;

public class ProductListRecyclerViewAdapter extends RecyclerView.Adapter<ProductListRecyclerViewAdapter.ViewHolder> {

    private final List<Product> mProductList;
    private final ProductListFragment.OnCartItemChangeListener mListener;

    public ProductListRecyclerViewAdapter(List<Product> items, ProductListFragment.OnCartItemChangeListener listener) {
        mProductList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Product product = mProductList.get(position);
        holder.mProduct = product;
        holder.mImageViewIcon.setImageResource(mProductList.get(position).getImageId());
        holder.mTextViewProductName.setText(mProductList.get(position).getName());
        holder.mTextViewUnit.setText(mProductList.get(position).getUnit());
        holder.mTextViewPrice.setText(Constants.GBP_SYMBOL +String.valueOf(mProductList.get(position).getPrice()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Notify the fragment callbacks interface that an item has been clicked
            }
        });

        holder.mNumberPickerProductCounter.setValue(Cart.getCart().getProductsQuantity(product));
        holder.mNumberPickerProductCounter.setOnCounterValueChangeListener(new NumberPicker.OnCounterValueChangeListener() {
            @Override
            public void onCounterValueChange(int newCounterValue) {
                mListener.onCartItemChange(holder.mProduct, newCounterValue);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageViewIcon;
        public final TextView mTextViewProductName;
        public final TextView mTextViewPrice;
        public final TextView mTextViewUnit;
        public final NumberPicker mNumberPickerProductCounter;
        public Product mProduct;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageViewIcon = (ImageView) view.findViewById(R.id.imageview_icon);
            mTextViewProductName = (TextView) view.findViewById(R.id.textview_product_name);
            mTextViewPrice = (TextView) view.findViewById(R.id.textview_total_amount);
            mTextViewUnit = (TextView) view.findViewById(R.id.textview_unit);
            mNumberPickerProductCounter = (NumberPicker) view.findViewById(R.id.number_picker_item_counter);
        }
    }
}
