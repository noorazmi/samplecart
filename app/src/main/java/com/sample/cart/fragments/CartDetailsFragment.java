package com.sample.cart.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sample.cart.R;
import com.sample.cart.adapters.CartProductListAdapter;
import com.sample.cart.models.Cart;
import com.sample.cart.models.Product;
import com.sample.cart.utils.Cashier;
import com.sample.cart.utils.Constants;
import com.sample.cart.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartDetailsFragment extends Fragment {

    private TextView mTextViewTotalItems;
    private TextView mTextViewTotalAmount;
    private ListView mListViewProductList;
    private double mTotalCartAmountInGBP;

    //Footer items at list footer view
    private TextView mTextViewTotalItemsFooter;
    private TextView mTextViewTotalAmountFooter;
    private TextView mTextViewTotalUSDAmountFooter;

    /* Keep track of if the ConverterTask is already running*/
    private boolean isConverterTaskRunning = false;
    private ProgressDialog mProgressDialog;
    private double mGBPToUSDConversionRate;


    public CartDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cart_details, container, false);
        Button buttonPayNow = (Button) rootView.findViewById(R.id.button_pay_now);
        mTextViewTotalItems = (TextView) rootView.findViewById(R.id.textview_items);
        mListViewProductList = (ListView) rootView.findViewById(R.id.listview);
        mTextViewTotalAmount = (TextView) rootView.findViewById(R.id.textview_total);
        buttonPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), getString(R.string.implement_payment_method_here), Toast.LENGTH_SHORT).show();
            }
        });
        addListViewFooter(inflater);
        return rootView;
    }

    /* Add details of the amount at the bottom of the product list */
    private void addListViewFooter(LayoutInflater inflater) {
        View footerViewAmountDetails = inflater.inflate(R.layout.cart_amount_details, null);
        mTextViewTotalItemsFooter = (TextView) footerViewAmountDetails.findViewById(R.id.textview_items_footer);
        mTextViewTotalAmountFooter = (TextView) footerViewAmountDetails.findViewById(R.id.textview_total_footer);
        mTextViewTotalUSDAmountFooter = (TextView) footerViewAmountDetails.findViewById(R.id.textview_total_in_usd);
        Button buttonConvert = (Button) footerViewAmountDetails.findViewById(R.id.button_convert);
        buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTotalCartAmountInGBP > 0){
                 fetchGBPToUSDConversionRate();
                }else {
                    Toast.makeText(getActivity(), getString(R.string.cart_empty_message), Toast.LENGTH_SHORT).show();
                }
            }
        });
        mListViewProductList.addFooterView(footerViewAmountDetails);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        updateAll();
        fetchGBPToUSDConversionRate();
    }

    private void updateAll() {
        setAdapter();
        updateProductsQuantity();
        updateTotalGBPAmount();
        updateTotalUSDAmount();
    }

    private void setAdapter() {

        CartProductListAdapter cartProductListAdapter = new CartProductListAdapter(getActivity(), getProductList(), new ProductListFragment.OnCartItemChangeListener() {
            @Override
            public void onCartItemChange(Product product, int quantity) {
                Cart.getCart().changeProductQuantity(product, quantity);
                if (quantity == 0) {
                    updateAll();
                } else {
                    updateProductsQuantity();
                    updateTotalGBPAmount();
                    updateTotalUSDAmount();
                }
            }
        });
        mListViewProductList.setAdapter(cartProductListAdapter);
    }

    private List<Product> getProductList() {
        Cart cart = Cart.getCart();
        return cart.getCartProductsList();
    }


    private void updateProductsQuantity() {
        Cart cart = Cart.getCart();
        int totalItems = cart.getTotalQuantity();
        String items =  String.valueOf(totalItems);
        mTextViewTotalItems.setText("Items: " +items);
        mTextViewTotalItemsFooter.setText(items);
    }

    private void updateTotalGBPAmount() {
        mTotalCartAmountInGBP = Cashier.calculateTotal(Cart.getCart());
        double totalGBPRoundOffAmount = Utils.getRoundOffValue(mTotalCartAmountInGBP);

        String amountText = "£" + String.valueOf(totalGBPRoundOffAmount);
        mTextViewTotalAmount.setText("Amount: "+amountText);
        mTextViewTotalAmountFooter.setText(amountText);
    }

    private void updateTotalUSDAmount() {
        double totalCartAmountInUSD = mGBPToUSDConversionRate * mTotalCartAmountInGBP;
        double roundOffValue =  Utils.getRoundOffValue(totalCartAmountInUSD);
        mTextViewTotalUSDAmountFooter.setText(Constants.USD_SYMBOL + String.valueOf(roundOffValue));
    }

    /* Bring the fresh conversion rate and show the usd cart amount */
    private void fetchGBPToUSDConversionRate() {

        if (!Utils.isInternetConnectionAvailable(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.no_internet_message), Toast.LENGTH_SHORT).show();
            return;
        }

        if (isConverterTaskRunning) {
            Toast.makeText(getActivity(), getString(R.string.conversinon_is_underprocess), Toast.LENGTH_SHORT).show();
        } else {
            mProgressDialog.show();
            new ConverterTask().execute();
        }
    }

    private class ConverterTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            isConverterTaskRunning = true;
            URL url;
            HttpURLConnection urlConnection = null;
            String jsonString = null;
            InputStream inputStream = null;

            try {
                url = new URL("http://api.fixer.io/latest?base=GBP");
                urlConnection = (HttpURLConnection) url.openConnection();
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                jsonString = convertStreamToString(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if(urlConnection != null){
                    urlConnection.disconnect();
                }


                //Close the input stream and release the resources
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return jsonString;
        }

        @Override
        protected void onPostExecute(String jsonString) {

            isConverterTaskRunning = false;
            mProgressDialog.dismiss();
            if (jsonString == null) {
                return;
            }
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONObject ratesJsonObject = jsonObject.getJSONObject("rates");
                mGBPToUSDConversionRate = ratesJsonObject.optDouble(Constants.CURRENCY_USD);
                updateTotalUSDAmount();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
