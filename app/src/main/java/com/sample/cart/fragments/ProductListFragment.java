package com.sample.cart.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sample.cart.R;
import com.sample.cart.activities.HomeActivity;
import com.sample.cart.adapters.ProductListRecyclerViewAdapter;
import com.sample.cart.models.Cart;
import com.sample.cart.models.Product;
import com.sample.cart.utils.Cashier;
import com.sample.cart.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class ProductListFragment extends Fragment {

    //Number of columns in the recycler view
    private RecyclerView mRecyclerView;
    private TextView mTextViewTotalItems;
    private TextView mTextViewTotalAmount;
    private List<Product> mProductList;

    /* Mandatory empty constructor for the fragment manager to instantiate thefragment (e.g. upon screen orientation changes).*/
    public ProductListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        mTextViewTotalItems = (TextView) rootView.findViewById(R.id.textview_items);
        mTextViewTotalAmount = (TextView) rootView.findViewById(R.id.textview_total);

        // Set the adapter
        Context context = rootView.getContext();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        Button buttonCheckout = (Button) rootView.findViewById(R.id.button_checkout);
        buttonCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart cart = Cart.getCart();
                if(cart == null){
                    showSnackbar(view, getString(R.string.cart_empty_message));
                    return;
                }
                int totalItems = cart.getTotalQuantity();
                if(totalItems >= 1){
                    ((HomeActivity)getActivity()).attachCartDetailsFragment();
                }else {
                    showSnackbar(view, getString(R.string.cart_empty_message));
                }
            }
        });

        return rootView;
    }

    private void showSnackbar(View view,String message){
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateAll();
    }

    public void updateAll(){
        setAdapter();
        updateQuantity();
        updateTotalAmount();
    }

    private void setAdapter(){
        mRecyclerView.setAdapter(new ProductListRecyclerViewAdapter(getProductsList(), mOnCartItemChangeListener));
    }

    private List<Product> getProductsList() {
        if(mProductList != null){
            return  mProductList;
        }
        mProductList = new ArrayList<>(4);
        Product product = new Product("Peas", 95, R.drawable.peas, "1 Bag", "sku_peas");
        mProductList.add(product);

        product = new Product("Eggs", 2.10f, R.drawable.egg, "1 Dozen", "sku_egg");
        mProductList.add(product);

        product = new Product("Milk", 1.30f, R.drawable.milk, "1 Bottle", "sku_milk");
        mProductList.add(product);

        product = new Product("Beans", 73, R.drawable.beans, "1 Can", "sku_beans");
        mProductList.add(product);

        return mProductList;
    }


    private final OnCartItemChangeListener mOnCartItemChangeListener = new OnCartItemChangeListener() {
        @Override
        public void onCartItemChange(Product product, int quantity) {
            Cart.getCart().changeProductQuantity(product, quantity);
            updateQuantity();
            updateTotalAmount();
        }
    };

    private void updateQuantity(){
        Cart cart = Cart.getCart();
        int totalItems = cart.getTotalQuantity();
        mTextViewTotalItems.setText("Items: "+String.valueOf(totalItems));
    }

    private void updateTotalAmount(){
        double totalAmount = Cashier.calculateTotal(Cart.getCart());
        mTextViewTotalAmount.setText("Amount: £"+String.valueOf(Utils.getRoundOffValue(totalAmount)));
    }



    /* Interface to get the call of item click on the recycler view */
    public interface OnCartItemChangeListener {
        void onCartItemChange(Product item, int quantity);
    }
}
