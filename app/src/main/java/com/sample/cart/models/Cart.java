package com.sample.cart.models;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class representing the cart, we can add and remove the items from the cart
 */

/* Singleton class Cart.  In user point view there will be only once cart per application launce and user can add, edit and remove the items in the cart*/
public class Cart {
    /**
     * An HashMap of cartItems items.
     */
    private final Map<Product, Integer> mCartItemsHasMap;
    private static Cart sCartInstance;

    private Cart() {
        mCartItemsHasMap = new HashMap<>();
    }

    public static Cart getCart(){
        if(sCartInstance == null){
            sCartInstance = new Cart();
        }

        return sCartInstance;
    }

    public void changeProductQuantity(Product product, int quantity){
        //If the count of the product became zero, remove the item from the cart and return, else add new quantity in the cart
        if(quantity == 0 && mCartItemsHasMap.containsKey(product)){
            mCartItemsHasMap.remove(product);
            return;
        }
        mCartItemsHasMap.put(product, quantity);
        Log.d("TAG", "changeProductQuantity() returned: " + mCartItemsHasMap.toString());
    }

    public List<Product> getCartProductsList() {
        return new ArrayList<>(mCartItemsHasMap.keySet());
    }

    public int getTotalQuantity(){

        int totalQuantity = 0;

        for (Integer items : mCartItemsHasMap.values()) {
            totalQuantity = totalQuantity + items;
        }

        return totalQuantity;
    }

    public int getProductsQuantity(Product product){
        if (mCartItemsHasMap.containsKey(product)) {
            return mCartItemsHasMap.get(product);
        }else {
            return 0;
        }
    }

    public Map<Product, Integer> getCartItemsHashMap() {
        return mCartItemsHasMap;
    }
}
