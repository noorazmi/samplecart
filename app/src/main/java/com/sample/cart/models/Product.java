package com.sample.cart.models;

import com.sample.cart.utils.Constants;

/**
 * <p/>
 * Created by: Noor  Alam on 02/06/16.<br/>
 * <p/>
 */
public class Product {

    private final String name;
    private final float price;
    private final String currency;
    private final int imageId;
    private final String unit;
    private final String sku;


    public Product(String name, float price, int imageId, String unit, String sku) {
        this.name = name;
        this.price = price;
        this.imageId = imageId;
        this.unit = unit;
        this.sku = sku;
        //Set the default currency. All products will have default currency GBP and user can not change or set it.
        this.currency = Constants.CURRENCY_GBP;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getImageId() {
        return imageId;
    }

    public String getUnit() {
        return unit;
    }

}
