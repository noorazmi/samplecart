package com.sample.cart.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * <p/>
 * Created by: Noor  Alam on 07/06/16.<br/>
 * Email id: noor.alam@tothenew.com<br/>
 * Skype id: mfsi_noora
 * <p/>
 */
public class Utils {
    private Utils() {
    }

    /**
     * Check Internet connection ,Return True If connected ,Otherwise False
     *
     * @param context Context
     * @return bool boolean
     */
    public static boolean isInternetConnectionAvailable(Context context) {
        if (context != null) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (NetworkInfo anInfo : info)
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }

            }
        }

        return false;
    }

    public static double getRoundOffValue(double input){
        return Math.round(input * 100.0) / 100.0;
    }

}
