package com.sample.cart.utils;

/**
 * <p/>
 * Created by: Noor  Alam on 04/06/16.<br/>
 * Email id: noor.alam@tothenew.com<br/>
 * Skype id: mfsi_noora
 * <p/>
 * <p/>
 * This class holds the app wide usable constants
 */
public class Constants {

    /*Prevent it from initialization. This class only to hold constants*/
    private Constants() {}

    public static final String CURRENCY_GBP = "GBP";
    public static final String CURRENCY_USD = "USD";

    public static final String GBP_SYMBOL = "£";
    public static final String USD_SYMBOL = "$";

}
