package com.sample.cart.utils;

import com.sample.cart.models.Cart;
import com.sample.cart.models.Product;

import java.util.Iterator;
import java.util.Map;

/**
 * <p/>
 * Created by: Noor  Alam on 04/06/16.<br/>
 * Email id: noor.alam@tothenew.com<br/>
 * Skype id: mfsi_noora
 * <p/>
 */

/* Class for calculating the total amount of the items in the cart*/
public class Cashier {

    public static double calculateTotal(Cart cart) {
        double total = 0f;
        Map<Product, Integer> cartHashMap = cart.getCartItemsHashMap();
        Iterator it = cartHashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Product product = (Product) pair.getKey();
            Integer items = (Integer) pair.getValue();
            total = total + product.getPrice() * items;
            //it.remove(); // avoids a ConcurrentModificationException
        }
        return total;
    }

}
