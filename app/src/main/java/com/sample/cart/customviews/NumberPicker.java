/*
 * Copyright (c) 2010, Jeffrey F. Cole
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 	Redistributions of source code must retain the above copyright notice, this
 * 	list of conditions and the following disclaimer.
 * 
 * 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 * 
 * 	Neither the name of the technologichron.net nor the names of its contributors 
 * 	may be used to endorse or promote products derived from this software 
 * 	without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.sample.cart.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sample.cart.R;


/**
 * A simple layout group that provides a numeric text area with two buttons to
 * increment or decrement the value in the text area. Holding either networkImageView
 * will auto increment the value up or down appropriately.
 */
public class NumberPicker extends LinearLayout {

    private final long REPEAT_DELAY = 50;

    private final int ELEMENT_HEIGHT = 30;
    private final int TEXT_SIZE = 18;
    private int MINIMUM = 0;
    private final int MAXIMUM = 100;
    private Integer value;
    private EditText valueText;
    private Button decrement;
    private Button increment;
    private final Handler repeatUpdateHandler = new Handler();

    private boolean autoIncrement = false;
    private boolean autoDecrement = false;
    private OnCounterValueChangeListener mOnCounterValueChangeListener;

    public NumberPicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        int width = (int) getResources().getDimension(R.dimen.background_drawable_rectange);
        int height = (int) getResources().getDimension(R.dimen.background_drawable_rectange);
        this.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, height));
        LayoutParams elementParams = new LayoutParams(width, height);

        // init the individual elements
        initDecrementButton(context);
        initValueEditText(context);
        initIncrementButton(context);

        // Can be configured to be vertical or horizontal
        // Thanks for the help, LinearLayout!
        if (getOrientation() == VERTICAL) {
            addView(increment, elementParams);
            addView(valueText, new LayoutParams(width, height));
            addView(decrement, elementParams);
        } else {
            addView(decrement, elementParams);
            addView(valueText, new LayoutParams(width, height));
            addView(increment, elementParams);
        }
    }

    private void initIncrementButton(final Context context) {
        increment = new Button(context);
        increment.setTextSize(TEXT_SIZE);
        increment.setTextColor(context.getResources().getColor(R.color.text_selector_white));
        increment.setBackgroundResource(R.drawable.sizeview_selector);
        increment.setGravity(Gravity.CENTER);
        increment.setTypeface(Typeface.DEFAULT_BOLD);
        increment.setText("+");

        increment.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (MAXIMUM == value) {
                    Toast.makeText(context, "Reached Maximum Quantity", Toast.LENGTH_SHORT).show();
                }
                increment();
            }
        });

        // Auto increment for a long click
        increment.setOnLongClickListener(
                new OnLongClickListener() {
                    public boolean onLongClick(View arg0) {
                        autoIncrement = true;
                        repeatUpdateHandler.post(new RepetetiveUpdater());
                        return false;
                    }
                }
        );

        // When the networkImageView is released, if we're auto incrementing, stop
        increment.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
                    autoIncrement = false;
                }
                return false;
            }
        });
    }

    private void initValueEditText(final Context context) {

        value = 0;
        valueText = new EditText(context);
        valueText.setTextSize(TEXT_SIZE);
        valueText.setBackgroundResource(R.drawable.sizeview_selector);
        valueText.setTextColor(context.getResources().getColor(R.color.ASSecondaryTeal));
        valueText.setEnabled(false);


        // Since we're a number that gets affected by the networkImageView, we need to be
        // ready to change the numeric value with a simple ++/--, so whenever
        // the value is changed with a keyboard, convert that text value to a
        // number. We can set the text area to only allow numeric input, but
        // even so, a carriage return can get hacked through. To prevent this
        // little quirk from causing a crash, store the value of the internal
        // number before attempting to parse the changed value in the text area
        // so we can revert to that in case the text change causes an invalid
        // number
        valueText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int arg1, KeyEvent event) {
                int backupValue = value;
                try {
                    value = Integer.parseInt(((EditText) v).getText().toString());
                } catch (NumberFormatException nfe) {
                    value = backupValue;
                }
                return false;
            }
        });
        valueText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(valueText.getWindowToken(), 0);

            }
        });
        valueText.setGravity(Gravity.CENTER | Gravity.BOTTOM);

        valueText.setText(value.toString());
        valueText.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    private void initDecrementButton(Context context) {
        decrement = new Button(context);
        decrement.setTextSize(TEXT_SIZE);
        decrement.setTextColor(context.getResources().getColor(R.color.text_selector_white));
        decrement.setBackgroundResource(R.drawable.sizeview_selector);
        decrement.setText("-");
        decrement.setTypeface(Typeface.DEFAULT_BOLD);
        decrement.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                decrement();
            }
        });


        decrement.setOnLongClickListener(
                new OnLongClickListener() {
                    public boolean onLongClick(View arg0) {
                        autoDecrement = true;
                        repeatUpdateHandler.post(new RepetetiveUpdater());
                        return false;
                    }
                }
        );

        // When the networkImageView is released, if we're auto decrementing, stop
        decrement.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP && autoDecrement) {
                    autoDecrement = false;
                }
                return false;
            }
        });
    }

    private void increment() {
        if (value < MAXIMUM) {
            value = value + 1;
            valueText.setText(value.toString());
            informChangeListener();
        }
    }

    private void informChangeListener() {
        if (mOnCounterValueChangeListener != null) {
            mOnCounterValueChangeListener.onCounterValueChange(value);
        }
    }

    public void setMinimumValue(int minimumValue) {
        MINIMUM = minimumValue;
    }

    private void decrement() {
        if (value > MINIMUM) {
            value = value - 1;
            valueText.setText(value.toString());
            informChangeListener();
        }
    }

    public void setValue(int value) {
        if (value > MAXIMUM) value = MAXIMUM;
        if (value > 0) {
            this.value = value;
            valueText.setText(this.value.toString());
        }
    }

    /**
     * This little guy handles the auto part of the auto incrementing feature.
     * In doing so it instantiates itself. There has to be a pattern name for
     * that...
     *
     * @author Jeffrey F. Cole
     */
    private class RepetetiveUpdater implements Runnable {
        public void run() {
            if (autoIncrement) {
                increment();
                repeatUpdateHandler.postDelayed(new RepetetiveUpdater(), REPEAT_DELAY);
            } else if (autoDecrement) {
                decrement();
                repeatUpdateHandler.postDelayed(new RepetetiveUpdater(), REPEAT_DELAY);
            }
        }
    }


    public void setOnCounterValueChangeListener(OnCounterValueChangeListener onCounterValueChangeListener) {
        this.mOnCounterValueChangeListener = onCounterValueChangeListener;
    }

    public interface OnCounterValueChangeListener {
        void onCounterValueChange(int newValue);
    }

}
